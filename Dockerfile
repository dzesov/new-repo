FROM openjdk:8

RUN mkdir -p /apps
COPY ./sagan-site/build/libs/sagan-site-1.0.0.BUILD-SNAPSHOT.jar /apps/app.jar
COPY ./entrypoint.sh /apps/entrypoint.sh


RUN chmod +x /apps/entrypoint.sh
ENTRYPOINT ["bash", "/apps/entrypoint.sh"]